
\documentclass[10pt, a4paper]{article}

\usepackage{csvsimple}
\usepackage{changepage}
\usepackage{cclicenses}
\usepackage{pbox}
%\usepackage[document]{ragged2e}
\usepackage{xcolor,colortbl}
\usepackage{url}
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc} % make weird characters work
\usepackage{graphicx}
\usepackage{geometry}
\usepackage{amsmath}
\usepackage[justification=centering]{caption}
\usepackage{enumitem}

\graphicspath{{slike/}}

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=black,filecolor=green,linkcolor=blue,urlcolor=blue}

\usepackage[english,serbian]{babel}

\usepackage{datetime}
\newdateformat{specialformat}{\twodigit{\THEDAY}.\twodigit{\THEMONTH}.\THEYEAR.}
\date{\specialformat\today}

\usepackage{listings}
\usepackage{float}
\restylefloat{table}
\usepackage{natbib}

\lstset{}

\begin{document}
\newpage
\title{Čvrsto savitljivi origami - uslovi i forsirajući skupovi \\ \small{Seminarski rad u okviru kursa Geometrijski algoritmi \\ Matematički fakultet}}
\author{Student: Jelena Jeremić, 1099/2021\\ Profesor: dr Predrag Janičić}
\maketitle
\renewcommand\abstractname{Sažetak}
\renewcommand\contentsname{Sadržaj}

\begin{abstract}
Čvrsta savitljivost je svojstvo origamija koje nam omogućava stvaranje trodimenzionalne strukture iz dvodimenzionalnih listova kroz proces savijanja isključivo duž već iscrtanih nabora (ivica). Primena ovog svojstva je od velike važnosti u oblastima robotike, projektovanja mehaničkih sklopova (na primer solarnih panela u svemiru), konstrukcije biomedicinskih uređaja i još mnogim inženjerskim disciplinama.\newline
Rad predstavlja saradnju osam naučnika: Zachary Abel, Jason Cantarella, Erik D. Demaine, David Eppstein, Thomas C. Hull, Jason S. Ku, Robert J. Lang i Tomohiro Tachi. Njihove uže oblasti istraživanja su računarske nauke i informatika, ali i matematika presavijanja papira, teorije origamija i proučavanje origamija iz interdisciplinarne perspektive. Cilj rada je pokazati koji su potrebni i dovoljni uslovi da bi se jednotemenski šablon ivica origamija, preslikan u planinsko - dolinski šablon ivica, mogao čvrsto saviti. Takodje, obradjena je tema samosklopivih origamija tj. nalaženja minimalnog podskupa ivica čijim savijanjem ostatak ivica možemo naterati da se sam savije do željenog oblika.\newline
Rad je objavljen u časopisu {\em{Journal of Computational Geometry}} 2016. godine pod nazivom {\em{Rigid Origami Vertices: Conditions and Forcing Sets}} \cite{article}.
\end{abstract}

\tableofcontents
\newpage

\section{Uvod}
\label{sec:uvod}

Čvrsti origami predstavlja granu origamija. Ono što je karakteristično za ovu vrstu je da su stranice nesavitljive (krute), a ivice koje spajaju ove stranice predstavljaju jedini savitljiv deo tj. imaju ulogu šarki između stranica.  Ova vrsta origamija ima brojne primene, neki od najznačajnijih primera su: kinetička arhitektura, izgradnja solarnih panela i robotika. Problem koji je predstavljen u ovom radu naziva se \textbf{čvrsta savitljivost} (eng. ~{\em rigid foldability}) i bavi se proučavanjem neophodnih i dovoljnih uslova koje šablon ivica mora ispuniti kako bi se ovakav origami mogao sklopiti. Cilj je transformisati materijal iz dvodimenzionalnog, planarnog stanja u trodimenzionalno, savijeno stanje u prostoru, ali tako da se nijedna stranica ne deformiše tokom procesa. Koristeći ove uslove, može se dokazati tvrđenje koje može poslužiti kao osnovna teorema za čvrsto savitljiv origami (kao što su teoreme Kavasakija i Maekave za ravno savitljive origamije\footnote{Više o ovoj temi možete pročitati ovde: \url{http://origametry.net/papers/fvfsequence.pdf}}). Na kraju će se rezultati rada primeniti na pronalaženje \textbf{\em{minimalnog forsirajućeg skupa}} (eng. ~{\em minimal forcing set}) tj. podskupa ivica čijim savijanjem od nekog trenutka možemo jedinstveno odrediti na koji način je potrebno saviti preostale ivice kako bi se dobio očekivani oblik \cite{doi:10.1098/rspa.2019.0215}.

\subsection{Bitni pojmovi}
\label{subsec: podnaslov1}

Posmatrajući region A $\subseteq$ {${R^2}$} (koji često nazivamo papirom, mada se mogu koristiti i drugi materijali), uočavamo šablon nabora (u daljem tekstu označen sa C) koji se može predstaviti kao \textbf{\em{planarni pravolinijski graf}} kojim se region A deli na čvorove, ivice i stranice (lica). Formiranje čvrstog origamija je time samo injektivno preslikavanje \textbf{\em{f: A \to {${R^3}$}}}. Svakoj liniji nabora (ivici) se dodeljuje broj {\boldmath{$p_{i}$}} koji označava za koliko stepeni će ta ivica biti savijena. Ako se šablon sastoji od n ivica, onda nam n-torka (${p_1}$, …, ${p_n}$) daje informaciju o tome za koliko stepeni će svaka ivica biti savijena odnosno daje nam \textbf{konfiguracioni prostor} \cite{doi:10.1098/rspa.2019.0215}. Pritom, ugao ne sme biti {$\pi$} ili -{$\pi$} jer je {\em{f}} injektivno preslikavanje, ali možemo uzeti vrednosti približno broju {$\pi$} \cite{20.500.11850/420478} \cite{article}.
\newline
\textbf{\em{Jednotemenski čvrsti origami}} označava da se sve linije nabora seku u jednoj tački u unutrašnjosti regiona A i jedan je od najjednostavnijih vrsta šablona origamija.
Fundamentalni problem je na koliko načina se, posmatrajući šablon ivica, moze izvršiti savijanje. U odgovoru na ovo pitanje nam pomaže planinsko - dolinsko označavanje šablona ivica. Definisaćemo preslikavanje \textbf{\em{\boldmath{$\mu$}: C \to {$\left\{M, V\right\}$
}}} kojim svakoj ivici šablona dodeljujemo jednu od dve moguće orijentacije - ispupčenu (planinsku, M) ili udubljenu (dolinsku, V). Za ivice koje će biti označene kao ispupčenja kažemo da imaju negativan ugao savijanja, a ivice označene kao udubljenja će imati pozitivan ugao savijanja.
\newline

\section{Definicije i glavna teorema}
\label{sec:naslov2}
U ovom odeljku predstavićemo potrebne i dovoljne uslove kako bi se jednotemenski šablon nabora mogao transformisati iz planarnog, nesavijenog stanja u savijeno, a koji se oslanjaju samo na unutrašnju geometriju šablona (nije bitan prostor u kome se šablon posmatra).
\newline
\textbf{\em{Definicija 1:}} Neka nam je dat jednotemenski šablon C i podela linija šablona na planine (M) i doline (V) koja se dobija primenom funkcije $\boldmath{\mu}$ nad C \cite{20.500.11850/420478} \cite{article}. Kažemo da par \textbf{(C, \boldmath{$\mu$})} sadrži:
\begin{itemize}
\item\textbf{planinski tronožac} - ako šablon, posmatrajući smer suprotan od kazaljke na satu, sadrži tri ivice {$c_1$}, {$c_2$}, {$c_3$} (ne moraju biti susedne) tako da \boldmath{$\mu$}({$c_i$}) \boldsymbol{= M} i važi da 0 < ({$c_i$}, {$c_{i+1}$}) < {$\pi$} za {\it{i}} = 1, 2, 3 (mod 3)
\item\textbf{dolinski tronožac} - jedina razlika od planinskog je da $\boldmath{\mu}$({$c_i$}) $\boldsymbol{= V}$ za {\it{i}} = 1, 2, 3 (mod 3)
\item\textbf{planinski krst} - ako šablon, posmatrajući smer suprotan od kazaljke na satu, sadrži četiri ivice {$c_1$}, {$c_2$}, {$c_3$}, {$c_4$} (ne moraju biti susedne) tako da {$c_1$} sa {$c_3$}, a tako i {$c_2$} sa {$c_4$}, gradi pravu liniju i da \boldmath{$\mu$}({$c_i$}) \boldsymbol{= M} i važi da 0 < ({$c_i$}, {$c_{i+1}$}) < {$\pi$} za {\it{i}} = 1, 2, 3, 4 (mod 4)
\item\textbf{dolinski krst}: jedina razlika od planinskog je da $\boldmath{\mu}$({$c_i$}) $\boldsymbol{= V}$ za {\it{i}} = 1, 2, 3, 4 (mod 4)
\end{itemize}
\textbf{\em{Definicija 2:}} Kažemo da šablon sadrži \textbf{,,ptičije stopalo"} ako sadrži tronožac ili krst i još jednu dodatnu liniju šablona koja se funkcijom {$\mu$} slika u suprotnu orijentaciju u odnosu na ivice koje čine tronožac ili krst. Ovo znači da ako su u pitanju planinski tronožac ili krst, tu dodatnu ivicu funkcija {$\mu$} mora slikati u dolinu i obrnuto.
\newline
Dolazimo do {\em{glavne teoreme}}.
\newline
\textbf{\em{Teorema 1:}} Jednotemenski šablon ivica označenih u vidu planina i dolina, može se čvrsto saviti iz početnog planarnog stanja ako i samo ako par \textbf{(C, \boldmath{$\mu$})} sadrži \textbf{,,ptičije stopalo"}. 
\newline
Interesantna posledica sledi iz glavne teoreme, ali pre nje navešćemo još jednu definiciju.
\newline
\textbf{\em{Definicija 3:}}  Neka je C jednotemenski šablon koji je čvrsto savitljiv i neka je C postavljen unutar ravni koja je upravna na ekvatorijalnu osu sfere, ali tako da osa prolazi kroz teme šablona C. Tada kažemo da je origami ispupčen ukoliko se sve ivice šablona savijaju prema donjoj polusferi, a udubljen ukoliko se ivice savijaju prema gornjoj polusferi \cite{hull_2020}.
\newline
\textbf{\em{Posledica}}: Ako jednotemenski šablon sadrži planinski tronožac ili krst, može se saviti tako da ivice budu ispupčene. Ako jednotemenski šablon sadrži dolinski tronožac ili krst, može se saviti tako da ivice budu udubljene.

\subsection{Uopštenje glavne teoreme}
\label{subsec:podnaslov2}
Postavlja se pitanje, da li je moguće izbeći korak preslikavanja ivica šablona u planine i doline? Odnosno, za zadat jednotemenski šablon C, da li je moguće samo prema rasporedu ivica koje se sastaju u temenu utvrditi da li je šablon čvrsto savitljiv?
\newline
Na ovo pitanje, odgovor nam daje sledeća teorema, koja zapravo predstavlja uopštenje glavne teoreme, ali pre nje uvešćemo još jednu definiciju.
\newline
\textbf{\em{Definicija 4:}} Neoznačen krst unutar jednotemenskog šablona C je krst koji formiraju četiri ivice kojima nije dodeljeno preslikavanje u planine i doline, ali tako da {$c_1$} sa {$c_3$} i {$c_2$} sa {$c_4$} formiraju prave linije. 
\newline
\textbf{\em{Teorema 2:}} Jednotemenski šablon C je čvrsto savitljiv ako i samo ako C ima najmanje četiri ivice, svi centralni uglovi između ivica su strogo manji od {$\pi$} i C nije ceo neoznačen krst.

\subsection{Forsirajući skupovi čvrsto savitljivih šablona}
\label{subsec:podnaslov3}
Forsirajući skup ivica nam daje kojim ivicama šablona je neophodno dodeliti planinsko-dolinsku orijentaciju. Nakon transformacije ivica tog skupa, preostalim ivicama šablona nije potrebno određivati orijentaciju jer će ih forsirajući skup ,,naterati'' da se saviju na jedinstven način kako bi se ceo šablon mogao čvrsto saviti.\newline
Za dat jednotemenski čvrsto savitljiv šablon ivica C sa temenom {$C_n$} (n predstavlja stepen čvora odnosno koliko se ivica sastaje u temenu) i funkciju {$\mu$} koja ivice slika u ispupčenja (M) i doline (V), neka funkcija \textbf{\em{f({$C_n$})}} označava \textbf{\em{minimalni forsirajući skup}} \cite{article} \cite{hull_2020}. Dakle, \textbf{\em{f({$C_n$})}} nam daje forsirajući skup koji sadrži najmanji broj ivica kojima je neophodno dodeliti orijentaciju i nijedan drugi forsirajući skup nema manju kardinalnost za dat šablon C. Koristeći navedene oznake, dolazimo do sledeće teoreme:\newline
\textbf{\em{Teorema 3:}} Za dat jednotemenski šablon C sa temenom {$C_n$} i stepena n, važi: \[ n-3 \leq |f({C_n})| \leq n,\quad za\quad n \geq 6 \]\[ 2 \leq |f({C_5})| \leq 4 \]\[ 1 \leq |f({C_4})| \leq 2 \]

\section{Primene}
\label{sec:naslov3}
Zbog toga što su ivice jedini savitljivi deo, svojstva ove vrste origamija su veoma korisna u dizajnu mašina i robota jer se ovde često koriste nesavitljivi materijali. Neke od najpoznatijih primera primena su {\em{,,Miura solarni paneli''}} \cite{primena2} gde je primenjen šablon savijanja japanskog astrofizičara Koyro Miura, kao i rešenje američkog fizičara Roberta J. Langa za sklapanje svemirskog teleskopa kako bi se mogao poslati u svemir \cite{primena}.


\section{Zaključak}
\label{sec:zakljucak}
Kroz rad je pokazano koji su potrebni i dovoljni uslovi kako bi se jednotemenski šablon ivica origamija mogao čvrsto saviti. Ono što je najbitnije je da ovi uslovi isključivo zavise od unutrašnje geometrije šablona, ne od prostora u kom se posmatra. Rezultati uslova su iskorišćeni i kako bi se prema ivicama origamija utvrdila veličina minimalnog forsirajućeg skupa ivica. Ovo otkriće je vrlo korisno za formiranje samosklopivih mikroskopskih mehanizama, sklopivih svemirskih struktura i kinetičkoj arhitekturi.  \newline
Ono što ipak ostaje kao otvoren problem je istraživanje čvrste savitljivosti za višetemenski šablon ivica origamija. Analiza ovakvog opšteg šablona ivica je NP težak problem i uslovi koji su dovoljni da se dokaže da je jednotemenski šablon čvrsto savitljiv nisu dovoljni u slučaju opšteg šablona.

\addcontentsline{toc}{section}{Literatura}
\bibliography{seminarski} 
\bibliographystyle{plain}
\appendix
\end{document}
